import pygame
from sys import exit

pygame.init()
screen = pygame.display.set_mode((800,600))
pygame.display.set_caption(f'Hell')
clock = pygame.time.Clock()
test_image_surface = pygame.image.load('backgrounds/sea.png').convert()
test_surface = pygame.Surface((40,40))
test_surface.fill((177, 98, 134))
font = pygame.font.Font("FantasqueSansMono Nerd Font.ttf", 50)
text_surface = font.render('Welcome!', True, (214, 93, 14))

while True:
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            pygame.quit()
            exit()
    screen.blit(test_image_surface, (0,0))
    screen.blit(test_surface, (200,400))
    screen.blit(text_surface, (300,200))
    pygame.display.update()
    clock.tick(60)
