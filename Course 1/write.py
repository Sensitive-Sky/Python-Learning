def progress_file_time(file):
    # 1. Keep variable initialization together with where its used
    time = ""
    with open(file, "w") as f:
        try: 
            time = input(f"Enter your progress in 'The ulimate introduction to pygame': ")
            if time == "exit": 
                print(f"Exiting...")
        except KeyboardInterrupt:
                print(f"\nExiting...")
        else:
            #This writes the file
            print(f"User Input: {time}")
            f.write(time)
#This runs the function and defines what the file is
progress_file_time(file="progress.txt")
