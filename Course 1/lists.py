wm = ["spectrwm", "openbox", "ratpoison", "qtile", "herbstluftwm", "awesome", "i3", "dwm", "bspwm", "xmonad", "stumpwm", "fluxbox", "twm", "jwm", "blackbox", "icewm", 16, True]

print(wm[1:3])
wm[10] = "StumpWM"
print(wm[10])

#List functions

os = ["Linux", "FreeBSD", "NetBSD", "OpenBSD", "GhostBSD"]
wm.extend(os)
wm.append("notion")
wm.insert(2, "mwm")
wm.remove("awesome")
#wm.clear()
wm.pop()
print(wm)
print(wm.index("ratpoison"))
print(wm.count("ratpoison"))
wm.remove(16)
wm.remove(True)
wm.sort()
print(wm)
os.reverse()
print(os)

wm2 = wm.copy()

print(wm2)
