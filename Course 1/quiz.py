class Question:
    def __init__(self, prompt, answer):
        self.prompt = prompt
        self.answer = answer


question_prompts = [ 
        "What doesn't travel on land?\n(A) Planes\n(B) Cars\n(C) Trucks",
        "What is the best dog?\n(A) Rex\n(B) Memes\n(C) Me",
        "What is Weed?\n(A) Breaking Bad\n(B) Leaves\n(C) Magic",
        "What is Breaking Bad?\n(A) Elepant Seal\n(B) Hell\n(C) Show about cooking",
        ]

questions = [
        Question(question_prompts[0], "A"),
        Question(question_prompts[1], "C"),
        Question(question_prompts[2], "B"),
        Question(question_prompts[3], "C"),
        ]

def run_test(questions):
    score = 0
    for question in questions:
        answer = input(question.prompt)
        if answer == question.answer:
            score += 1
    print("You got " + str(score) + "/" + str(len(questions)) + " correct!")

run_test(questions)
