from Classes import Student

student1 = Student("Jim", "Weed", 4.0, "Drinking", True)

print(student1.name)

print(student1.on_honor_roll())

class Programmer_Student(Student):
    def writes_in_python(self):
        return  "indents and modules go brrrr!"

Me = Programmer_Student("Me", "Computer Science", 3.8, "Music and tech", False)

print(Me.on_honor_roll())
print(Me.writes_in_python())
