shells = {
        "bash": "Bourne Again Shell",
        "zsh": "zshell",
        "ksh": "kornshell",
        3: "fish",
        }

#No dup keys in a dictionary
print(shells["bash"])
print(shells.get("zsh"))
#print(shells.get("fish", "Not a vaild key"))
print(shells[3])
