phrase = "sea doggo"
#         012345678
print(" Elephant\n Seal\n\"Sea Doggo\"")
print(phrase + " is a Seal")
print(phrase.upper())
print(phrase.lower())
print(phrase.upper().isupper())
print(phrase.upper().islower())
print(len(phrase))
print(phrase[2])
print(phrase.index("o"))
print(phrase.replace("sea", "grass"))
