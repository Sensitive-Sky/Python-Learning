secret_words = ["Python", "PYTHON", "python", "bagels"]

guesses = ""

exit = "exit"

guess_count = 0

guess_max = 3

try:
    while guesses != exit and guess_count < guess_max: 
        if guess_max > guess_count:
            guesses = input("Enter a guess! \nYou have three tries and type \"exit\" to exit: ")
        if  guesses != exit and guesses != "Python" and guesses != "PYTHON" and guesses != "python" and guesses != "bagels":
            guess_count+=1
            print("Hint: What Programming langauge is this file written in?")
            print(f"You have {str(guess_max - guess_count)} tries left.")
        if guess_count == 3 and guesses != ["Python", "PYTHON", "python", "bagels"]:
            print("Gameover Exiting... ")
            break
        if guesses in secret_words:
            print("Congrats! Have some cake!")
            break
        if guesses == exit:
            print("Exiting...")
            break
except EOFError:
    print("\nExiting...")
except KeyboardInterrupt:
    print("\nExiting...")
