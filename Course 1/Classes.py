class Student:
    
    def __init__(self, name, major, gpa, hobby, is_high_af):
        self.name = name
        self.major = major
        self.gpa = gpa
        self.hobby = hobby
        self.is_on_frat = is_high_af
 
    def on_honor_roll(self):
        if self.gpa >= 3.5:
            return True
        else:
            return False
