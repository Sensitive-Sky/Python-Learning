class snowy_house:
    frames = ["        / \ \n    *  /   \ \n  *   / *   \ \n    */   _ * \  *  \n *  /   |_|   \  \n   /  *      * \ \n  /_____________\ \n  |  *          | * \n* | [] */ \  [] |\n  | *   | |  *  |\n  |_____|_|_____|", "        / \ \n       /   \ \n      / *   \ \n     /   _   \     \n    /   |_|  *\  \n * /        *  \ \n */_____________\ \n  |         *   |   \n  | [] */ \  [] |\n  |     | |  *  |\n *|_____|_|_____|"]
    frame_index = 0.0
    speed = 0.2 
    def __init__(self, _speed):
        self.speed = _speed
    def animate(self):
        self.frame_index += self.speed
        if self.frame_index < 0:
            self.frame_index = float(len(self.frames)) - 1
        elif self.frame_index > float(len(self.frames)):
            self.frame_index = 0
    def get_frame(self):
        index = round(self.frame_index)
        return self.frames[max(0, round(self.frame_index) - 1)]
# -------

import subprocess

sprite = snowy_house(2.0)
try:
    while True:
        subprocess.run("clear")
        sprite.animate()
        print(sprite.get_frame())
        subprocess.run(["sleep", "0.1"])
except KeyboardInterrupt:
    print("\nExiting...")
